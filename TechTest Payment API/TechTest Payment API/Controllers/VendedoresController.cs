﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Numerics;
using System.Text.RegularExpressions;
using TechTest_Payment_API.Models;

namespace TechTest_Payment_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VendedoresController : ControllerBase
    {
        private readonly Contexto _contexto;

        public VendedoresController(Contexto contexto)
        {
            _contexto = contexto;
        }

        private bool ChecarCPF(string cpf) 
        {
            // Pega apenas os números da string
            cpf = string.Join("", cpf.ToCharArray().Where(Char.IsDigit));

            // Checa se tem 11 dígitos
            if (cpf.Length != 11)
                return false;

            return true;
        }
        private bool ChecarEmail(string email)
        {
            // Checa se possui @ e ., e também se não começa/termina neles
            if (email.Contains("@") && email.Contains(".")
                && !email.StartsWith("@") && !email.EndsWith("."))
                return true;

            return false;
        }
        private bool ChecarTelefone(string telefone)
        {
            // Pega apenas os números da string
            telefone = string.Join("", telefone.ToCharArray().Where(Char.IsDigit));

            // Checa se tem entre 13 digitos ou 11 digitos, DDD Regional ou Local
            if (telefone.Length == 11 | telefone.Length == 13)
                return true;

            return false;
        }

        [HttpGet]
        public IActionResult BuscarVendedores()
        {
            var dbVendedores = _contexto.Vendedores.ToList();
            if (dbVendedores.Count == 0)
                return NotFound("Não há vendedores registrados");

            return Ok(dbVendedores);
        }

        [HttpPost]
        public ActionResult<Vendedor> RegistrarVendedor(Vendedor vendedor)
        {
            if (!ChecarCPF(vendedor.CPF))
                return BadRequest("CPF Inválido");

            if (!ChecarEmail(vendedor.Email))
                return BadRequest("Email Inválido");

            if (!ChecarTelefone(vendedor.Telefone))
                return BadRequest("Telefone Inválido");

            _contexto.Vendedores.Add(vendedor);
            _contexto.SaveChanges();

            return Ok(vendedor);
        }
    }
}
