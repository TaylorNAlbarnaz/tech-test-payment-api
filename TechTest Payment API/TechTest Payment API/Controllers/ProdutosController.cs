﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Numerics;
using System.Text.RegularExpressions;
using TechTest_Payment_API.Models;

namespace TechTest_Payment_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProdutosController : ControllerBase
    {
        private readonly Contexto _contexto;

        public ProdutosController(Contexto contexto)
        {
            _contexto = contexto;
        }

        [HttpGet]
        public IActionResult BuscarProdutos()
        {
            var dbProdutos = _contexto.Produtos.ToList();
            if (dbProdutos.Count == 0)
                return NotFound("Não há produtos registrados");

            return Ok(dbProdutos);
        }

        [HttpPost]
        public ActionResult<Vendedor> RegistrarProduto(Produto produto)
        {
            if (produto.Nome == "")
                return BadRequest("Nome não pode ser vazio");

            _contexto.Produtos.Add(produto);
            _contexto.SaveChanges();

            return Ok(produto);
        }
    }
}
