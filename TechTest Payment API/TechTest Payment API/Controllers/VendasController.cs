﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TechTest_Payment_API.Models;

namespace TechTest_Payment_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VendasController : ControllerBase
    {
        private readonly Contexto _contexto;

        public VendasController(Contexto contexto)
        {
            _contexto = contexto;
        }

        private List<Produto> gerarProdutos(List<int> ids)
        {
            var produtos = new List<Produto>();

            for (var i=0; i<ids.Count; i++)
            {
                var dbProduto = _contexto.Produtos.Find(ids[i]);
                if (dbProduto != null)
                    produtos.Add(dbProduto);
            }

            return produtos;
        }
        
        private bool podeAtualizar(StatusVenda statusAnterior, StatusVenda statusNovo)
        {
            if (statusAnterior == StatusVenda.AguardandoPagamento)
            {
                if (statusNovo == StatusVenda.PagamentoAprovado)
                    return true;
                if (statusNovo == StatusVenda.Cancelada)
                    return true;
            }

            if (statusAnterior == StatusVenda.PagamentoAprovado)
            {
                if (statusNovo == StatusVenda.EnviadoParaTransportadora)
                    return true;
                if (statusNovo == StatusVenda.Cancelada)
                    return true;
            }

            if (statusAnterior == StatusVenda.EnviadoParaTransportadora
                && statusNovo == StatusVenda.Entregue)
                return true;

            return false;
        }

        [HttpGet("{id}")]
        public ActionResult<Venda> BuscarVenda(int id)
        {
            var dbVenda = _contexto.Vendas
                .Include(v => v.Vendedor)
                .Include(v => v.Produtos)
                .Where(v => v.Id == id);
            if (dbVenda == null)
                return NotFound("Venda não encontrada");

            return Ok(dbVenda);
        }

        [HttpPost]
        public IActionResult RegistrarVenda(VendaDto venda)
        {
            var dbVendedor = _contexto.Vendedores.Find(venda.VendedorId);
            if (dbVendedor == null)
                return BadRequest("Vendedor Inválido");

            if (venda.Produtos.Count == 0)
                return BadRequest("Lista de produtos não pode estar vazia!");

            var dbVenda = new Venda
            {
                Status = StatusVenda.AguardandoPagamento,
                VendedorId = venda.VendedorId,
                Vendedor = dbVendedor,
                Data = DateTime.Now
            };

            var produtos = gerarProdutos(venda.Produtos);
            if (produtos.Count == 0)
                return BadRequest("Lista de produtos inválida!");

            dbVenda.Produtos = produtos;

            _contexto.Vendas.Add(dbVenda);
            _contexto.SaveChanges();

            return Ok(dbVenda);
        }

        [HttpPut("{id}")]
        public IActionResult AtualizarVenda(int id, StatusVenda status)
        {
            var dbVenda = _contexto.Vendas.Find(id);
            if (dbVenda == null)
                return NotFound("Venda não encontrada!");

            if (!podeAtualizar(dbVenda.Status, status))
                return BadRequest("Status inválido para essa venda!");

            dbVenda.Status = status;

            _contexto.Vendas.Update(dbVenda);
            _contexto.SaveChanges();

            return Ok();
        }
    }
}
