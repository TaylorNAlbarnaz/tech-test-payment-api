﻿using Microsoft.EntityFrameworkCore;

namespace TechTest_Payment_API.Models
{
    public class Contexto : DbContext
    {
        public Contexto(DbContextOptions<Contexto> options) : base(options) { }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Relação one-to-many entre Vendas e Vendedores
            modelBuilder
                .Entity<Venda>()
                .HasOne(v => v.Vendedor)
                .WithMany()
                .IsRequired()
                .HasForeignKey(v => v.VendedorId);
        }

        public DbSet<Produto> Produtos => Set<Produto>();
        public DbSet<Venda> Vendas => Set<Venda>();
        public DbSet<Vendedor> Vendedores => Set<Vendedor>();
    }
}
