﻿using System.ComponentModel.DataAnnotations;

namespace TechTest_Payment_API.Models
{
    public class Venda
    {
        [Key]
        public int Id { get; set; }
        public DateTime Data { get; set; }
        public StatusVenda Status { get; set; }

        public int VendedorId { get; set; }
        public Vendedor Vendedor { get; set; }

        public List<Produto> Produtos { get; set; }
    }
}
