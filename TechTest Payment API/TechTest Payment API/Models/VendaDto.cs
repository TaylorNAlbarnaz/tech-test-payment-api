﻿using System.ComponentModel.DataAnnotations;

namespace TechTest_Payment_API.Models
{
    public class VendaDto
    {
        [Key]
        public int Id { get; set; }
        public int VendedorId { get; set; }
        public List<int> Produtos { get; set; }
    }
}
