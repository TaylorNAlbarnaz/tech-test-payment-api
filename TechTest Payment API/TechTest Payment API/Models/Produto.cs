﻿using System.ComponentModel.DataAnnotations;

namespace TechTest_Payment_API.Models
{
    public class Produto
    {
        [Key]
        public int Id { get; set; }
        public string Nome { get; set; }
    }
}
