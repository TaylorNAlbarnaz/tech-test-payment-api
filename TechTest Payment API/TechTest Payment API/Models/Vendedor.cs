﻿using System.ComponentModel.DataAnnotations;

namespace TechTest_Payment_API.Models
{
    public class Vendedor
    {
        [Key]
        public int Id { get; set; }
        public string CPF { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
    }
}
